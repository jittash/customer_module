export interface CustomerType {
    id: number | string;
    profileImage?: string;
    name: string;
    username?: string;
    email: string;
    phone: string | number | undefined;
  }
  
import React, { useState } from "react";
import { CustomerType } from "../../types";
import { Avatar, Button, Card, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Drawer, List, ListItem, ListItemAvatar, ListItemText, Snackbar, TextField, Tooltip, Typography } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import EditIcon from "@material-ui/icons/Edit";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  head: {
    display: "flex"
  },
  subHead: {
    marginLeft: "auto",
    display: "flex"
  },
  searchBar: {
    padding: "0.125rem",
    border: "0.125rem solid grey",
    borderRadius: "0.5rem"
  },
  paper: {
    width: "100%",
    backgroundColor: "grey"
  },
  rootCard: {
    fontSize: "2rem",
    width: "70%",
    padding: "1rem",
    display: "flex",
    flexDirection: "column",
    margin: "30vh auto 0 auto"
  },
  drawerIcon: {
    padding: "1rem 1rem 0 0",
    display: "flex",
    justifyContent: "flex-end"
  }
});

var users: Array<CustomerType> = [
  {
    id: 1,
    profileImage: "https://avatars.githubusercontent.com/u/1?v=4",
    name: "Tom Preston-Werner",
    username: "mojombo",
    email: "mojombo@gmail.com",
    phone: "9474965736"
  },
  {
    id: 2,
    profileImage: "https://avatars.githubusercontent.com/u/2?v=4",
    name: "Chris Wanstrath",
    username: "defunkt",
    email: "chris@gmail.com",
    phone: "9572524776"
  },
  {
    id: 3,
    profileImage: "https://avatars.githubusercontent.com/u/3?v=4",
    name: "PJ Hyett",
    username: "pjhyett",
    email: "hyett@gmail.com",
    phone: "7454676548"
  },
  {
    id: 4,
    profileImage: "https://avatars.githubusercontent.com/u/4?v=4",
    name: "Yehuda catz",
    username: "wycats",
    email: "yehuda@gmail.com",
    phone: "8464875736"
  },
  {
    id: 5,
    profileImage: "https://avatars.githubusercontent.com/u/5?v=4",
    name: "Ezra Zygmuntowicz",
    username: "ezmobius",
    email: "ezra@gmail.com",
    phone: "9837946586"
  },
  {
    id: 6,
    profileImage: "https://avatars.githubusercontent.com/u/6?v=4",
    name: "Michael D. Ivey",
    username: "ivey",
    email: "evan@gmail.com",
    phone: "9837946856"
  },
  {
    id: 7,
    profileImage: "https://avatars.githubusercontent.com/u/7?v=4",
    name: "Evan Phoenix",
    username: "evanphx",
    email: "ezra@gmail.com",
    phone: "9837867346"
  },
  {
    id: 17,
    profileImage: "https://avatars.githubusercontent.com/u/17?v=4",
    name: "Chris Van Pelt",
    username: "vanpelt",
    email: "vanpelt@gmail.com",
    phone: "7389946586"
  },
  {
    id: 18,
    profileImage: "https://avatars.githubusercontent.com/u/18?v=4",
    name: "Wayne E. Seguin",
    username: "wayneeseguin",
    email: "wayne@gmail.com",
    phone: "9837206490"
  },
  {
    id: 19,
    profileImage: "https://avatars.githubusercontent.com/u/19?v=4",
    name: "Bryan Helmkamp",
    username: "brynary",
    email: "bryan@gmail.com",
    phone: "9845689386"
  }
];

const App: React.FC = () => {
  const classes = useStyles();
  const [name, setname] = useState<string>("");
  const [email, setemail] = useState<string>("");
  const [mobile, setmobile] = useState<number | string>();

  const [customers, setcustomers] = useState<Array<CustomerType>>(users);
  const [cid, setcid] = useState<number | string>(0);
  const [toEdit, settoEdit] = useState<boolean>(false);
  const [addDialog, setaddDialog] = useState<boolean>(false);
  const [openDrawer, setopenDrawer] = useState<boolean>(false);
  const [confirmDelete, setconfirmDelete] = useState<boolean>(false);
  const [snackbar, setsnackbar] = useState<boolean>(false);

  const [searchValue, setsearchValue] = useState<any>("");
  const [searchResults, setsearchResults] = useState<any[]>([]);

  const handleOpen = (): void => {
    setaddDialog(true);
  };
  const handleClose = (): void => {
    setaddDialog(false);

    if (!toEdit) {
      setname("");
      setemail("");
      setmobile("");
    }
    settoEdit(false);
  };

  const handleDrawer = (key: number | string): void => {
    setcid(key);
    setopenDrawer(true);
    /*Picking up the detils of selected customer*/
    const selectedCustomer = customers.find((customer) => {
      return customer.id === key;
    });
    setname(selectedCustomer.name);
    setemail(selectedCustomer.email);
    setmobile(selectedCustomer.phone);
  };
  const closeDrawer = (): void => {
    setcid(0);
    setname("");
    setemail("");
    setmobile("");
    setopenDrawer(false);
  };

  const promptDelete = (): void => {
    setconfirmDelete(true);
  };
  const handleDelete = (): void => {
    setconfirmDelete(false);
  };

  const openSnack = (): void => {
    setsnackbar(true);
  };
  const closeSnack = (): void => {
    setsnackbar(false);
  };

  /* Add New or Edit Existing Customer */

  const addCustomer = (e: any) => {
    e.preventDefault();

    /* Edit Customer*/
    if (toEdit) {
      setcustomers(
        customers.map((customer) => {
          if (customer.id === cid) {
            return { ...customer, name, email, phone: mobile };
          }
          return customer;
        })
      );
      settoEdit(false);
      setopenDrawer(false);
      /* Add Customer */
    } else {
      const newCustomer: CustomerType = {
        id: new Date().getTime().toString(),
        name,
        email,
        phone: mobile
      };

      setcustomers([...customers, newCustomer]);
    }
    setname("");
    setemail("");
    setmobile("");
    setaddDialog(false);
  };

  /* Picking Customer object to edit */
  const editCustomer = (): void => {
    settoEdit(true);
    setaddDialog(true);
  };

  /* Delete an Existing Customer*/

  const successDelete = (): void => {
    setconfirmDelete(false);
    setopenDrawer(false);
    setcid(0);

    setcustomers(
      customers.filter((customer) => {
        return customer.id !== cid;
      })
    );

    /* Snackbar alert */
    return openSnack();
  };

  const handleSearch = (e: any) => {
    e.preventDefault();
    setsearchValue(e.target.value);
    const searchList = customers.filter((customer) => {
      return Object.values(customer)
        .join(" ")
        .toLowerCase()
        .includes(searchValue.toLowerCase());
    });
    setsearchResults(searchList);
  };

  return (
    <div className="App">
      <div className="main">
        <div className={classes.head}>
          <Typography variant="h5">Customers</Typography>

          <div className={classes.subHead}>
            <div className={classes.searchBar}>
              <SearchIcon />
              <TextField
                placeholder="Type to Search..."
                onChange={handleSearch}
              />
            </div>

            <Tooltip title="Add Customer">
              <AddCircleOutlineOutlinedIcon
                fontSize="large"
                onClick={handleOpen}
              />
            </Tooltip>
          </div>
        </div>

        {/* Add Customer Dialog */}

        <Dialog
          open={addDialog}
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            <Typography variant="h5">
              {toEdit ? "Update Customer" : "New Customer"}
            </Typography>
            <Tooltip title="Close">
              <CloseIcon onClick={handleClose} />
            </Tooltip>
          </DialogTitle>
          <DialogContent>
            <DialogContentText></DialogContentText>
            <TextField
              error
              autoFocus
              id="name"
              placeholder="Name"
              type="text"
              value={name}
              onChange={(event) => setname(event.target.value)}
              fullWidth
            />
            <TextField
              id="email"
              placeholder="Email"
              type="email"
              value={email}
              onChange={(event) => setemail(event.target.value)}
              fullWidth
            />
            <TextField
              id="number"
              placeholder="number"
              type="number"
              value={mobile}
              onChange={(event) => setmobile(event.target.value)}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button variant="contained" color="primary" onClick={addCustomer}>
              Save
            </Button>
          </DialogActions>
        </Dialog>

        {/* Customer Detail Drawer */}

        <Drawer
          open={openDrawer}
          onClose={closeDrawer}
          anchor="right"
          classes={{ paper: classes.paper }}
        >
          <div className={classes.drawerIcon}>
            <Tooltip title="Edit">
              <EditIcon onClick={editCustomer} />
            </Tooltip>
            <Tooltip title="Delete">
              <DeleteOutlineIcon onClick={promptDelete} />
            </Tooltip>
            <Tooltip title="Close">
              <CloseIcon onClick={closeDrawer} />
            </Tooltip>
          </div>

          <Card className={classes.rootCard}>
            <div>Name : {name} </div>
            <div>Email : {email} </div>
            <div>Phone : {mobile}</div>
          </Card>
        </Drawer>

        {/*Confirm Delete Dialog */}

        <Dialog
          open={confirmDelete}
          onClose={handleDelete}
          aria-labelledby="delete-dialog-title"
        >
          <DialogTitle id="delete-dialog-title">
            <Typography variant="h5">Delete Customer</Typography>
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Are you sure you want to continue?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDelete}>Cancel</Button>
            <Button variant="contained" color="primary" onClick={successDelete}>
              CONFIRM
            </Button>
          </DialogActions>
        </Dialog>

        {/* Delete Success Snackbar */}

        <Snackbar open={snackbar} onClose={closeSnack} autoHideDuration={4000}>
          <Alert severity="info">
            Customer deleted successfully!{" "}
            <span onClick={closeSnack}>DISMISS</span>
          </Alert>
        </Snackbar>

        {/* Listing Every Customer */}

        {customers.map((customer) => {
          return (
            <List key={customer.id}>
              <ListItem onClick={() => handleDrawer(customer.id)}>
                <ListItemAvatar>
                  <Avatar src={customer.profileImage} />
                </ListItemAvatar>
                <ListItemText
                  primary={customer.name}
                  secondary={customer.email}
                />
              </ListItem>
              <Divider variant="fullWidth" />
            </List>
          );
        })}
      </div>
    </div>
  );
};

export default App;
